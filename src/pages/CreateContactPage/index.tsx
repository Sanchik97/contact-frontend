import React from "react";

import { ContactForm } from "@/features/contactForm";

export interface IndexProps {}

const Index: React.FC<IndexProps> = () => {
  return (
    <>
      <h2>Create Contact</h2>
      <ContactForm />
    </>
  );
};

export default Index;
