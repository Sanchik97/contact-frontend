import React from "react";

import { Container } from "@/shared/ui/Container";
import { NotFound } from "@/shared/ui/NotFound";

interface props {}

const Index: React.FC<props> = () => {
  return (
    <Container>
      <NotFound />
    </Container>
  );
};

export default Index;
