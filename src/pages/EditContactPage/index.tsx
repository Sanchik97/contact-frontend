import React from "react";
import { useParams } from "react-router-dom";

import { ContactForm } from "@/features/contactForm";

export interface IndexProps {}

const Index: React.FC<IndexProps> = () => {
  const { id } = useParams<{ id: string }>();

  return (
    <>
      <h2>Edit Contact</h2>
      <ContactForm id={id} />
    </>
  );
};

export default Index;
