import React from "react";

import { AuthWidget } from "@/widgets/authWidget";

export interface IndexProps {}

const Index: React.FC<IndexProps> = () => {
  return (
    <React.Fragment>
      <AuthWidget />
    </React.Fragment>
  );
};

export default Index;
