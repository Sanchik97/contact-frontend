import React from "react";
import classNames from "classnames";

import { LogoutButton } from "@/features/logout";
import { Button } from "@/shared/ui/Button";
import { Container } from "@/shared/ui/Container";
import { ContactsFilters } from "@/widgets/contactsFilters";
import { ContactsList } from "@/widgets/contactsList";

import cn from "./HomePage.module.css";

interface props {}

const Index: React.FC<props> = () => {
  return (
    <React.Fragment>
      <Container>
        <div className={classNames(cn.split)}>
          <Button href={"/contacts/add"}>Add contact</Button>
          <LogoutButton />
        </div>
        <ContactsFilters />
        <ContactsList />
      </Container>
    </React.Fragment>
  );
};

export default Index;
