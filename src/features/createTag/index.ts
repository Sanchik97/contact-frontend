import { CreateTagModal, CreateTagModalProps } from "./ui/CreateTagModal";

export type { CreateTagModalProps };
export { CreateTagModal };
