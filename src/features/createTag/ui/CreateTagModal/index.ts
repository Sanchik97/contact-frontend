import CreateTagModal, { CreateTagModalProps } from "./CreateTagModal";

export type { CreateTagModalProps };
export { CreateTagModal };
