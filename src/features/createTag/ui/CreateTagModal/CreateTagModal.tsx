import React, { useState } from "react";

import { useCreateTagMutation } from "@/entities/Tag";
import { Button } from "@/shared/ui/Button";
import { FormField } from "@/shared/ui/FormField";
import { Input } from "@/shared/ui/Input";
import { Modal, ModalProps } from "@/shared/ui/Modal";

export interface CreateTagModalProps extends ModalProps {}

const CreateTagModal: React.FC<CreateTagModalProps> = () => {
  const [visible, setVisible] = useState(false);
  const handleVisibleChange = () => setVisible(!visible);
  const [tagName, setTagName] = useState("");
  const handleTagNameChange = (e: React.ChangeEvent<HTMLInputElement>) =>
    setTagName(e.target.value);

  const [mutation, state] = useCreateTagMutation();

  const onSubmit = async () => {
    try {
      await mutation(tagName).unwrap();
      handleVisibleChange();
    } catch (e) {
      console.error(e);
    }
  };

  return (
    <>
      <Button onClick={handleVisibleChange} block>
        Create tag
      </Button>
      <Modal visible={visible} handleVisibleChange={handleVisibleChange}>
        <FormField label={"Tag name"}>
          <Input
            value={tagName}
            onChange={handleTagNameChange}
            placeholder={"Input tag name..."}
            required
          />
        </FormField>
        <Button
          onClick={onSubmit}
          type={"submit"}
          theme={"secondary"}
          isLoading={state.isLoading}
          disabled={state.isLoading}
        >
          Create
        </Button>
      </Modal>
    </>
  );
};

export default CreateTagModal;
