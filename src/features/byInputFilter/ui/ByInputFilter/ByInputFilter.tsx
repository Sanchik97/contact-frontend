import React, { useEffect, useState } from "react";
import { useSearchParams } from "react-router-dom";

import { useDebounce } from "@/shared/lib/useDebounce";
import { Input } from "@/shared/ui/Input";

export interface ByInputFilterProps {}

const ByInputFilter: React.FC<ByInputFilterProps> = () => {
  const [searchParams, setSearchParams] = useSearchParams();
  const [value, setValue] = useState(searchParams.get("search") ?? "");
  const debounced = useDebounce(value);

  const handleSetSearch = (e: React.ChangeEvent<HTMLInputElement>) =>
    setValue(e.target.value);

  useEffect(() => {
    searchParams.set("search", debounced);
    setSearchParams(searchParams);
  }, [debounced, searchParams, setSearchParams]);

  return (
    <>
      <Input
        value={value}
        onChange={handleSetSearch}
        placeholder={"Filter by name, phone, email..."}
      />
    </>
  );
};

export default ByInputFilter;
