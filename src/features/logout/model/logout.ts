import { removeFromLocalStorage } from "@/shared/lib/localStorage";

export function logout() {
  removeFromLocalStorage("auth-token");
}
