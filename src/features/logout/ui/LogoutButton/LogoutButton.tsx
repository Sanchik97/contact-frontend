import React from "react";
import { useNavigate } from "react-router-dom";

import { logout } from "@/features/logout/model/logout";
import { Button, ButtonProps } from "@/shared/ui/Button";

export interface HandleLogoutProps extends ButtonProps {}

const LogoutButton: React.FC<HandleLogoutProps> = () => {
  const navigate = useNavigate();
  const handleLogout = () => {
    logout();
    navigate("/auth");
  };
  return (
    <Button theme={"secondary"} onClick={handleLogout}>
      Logout
    </Button>
  );
};

export default LogoutButton;
