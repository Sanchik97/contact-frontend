import React from "react";

import { useGetAllTagsQuery } from "@/entities/Tag";
import { Select, SelectProps } from "@/shared/ui/Select";

export interface SelectTagProps extends SelectProps {}

const SelectTag: React.FC<SelectTagProps> = (props) => {
  const { data } = useGetAllTagsQuery();
  return (
    <Select {...props}>
      <option value={undefined}>{props.placeholder}</option>
      {data?.map((tag) => (
        <option key={tag.id} value={tag.id}>
          {tag.name}
        </option>
      ))}
    </Select>
  );
};

export default SelectTag;
