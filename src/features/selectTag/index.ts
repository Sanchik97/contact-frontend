import { SelectTag, SelectTagProps } from "./model/ui/SelectTag";

export type { SelectTagProps };
export { SelectTag };
