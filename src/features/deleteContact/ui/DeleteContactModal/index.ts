import DeleteContactModal, {
  DeleteContactModalProps,
} from "./DeleteContactModal";

export type { DeleteContactModalProps };
export { DeleteContactModal };
