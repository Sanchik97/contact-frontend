import React, { useState } from "react";
import classNames from "classnames";

import { useDeleteContactMutation } from "@/entities/Contact";
import { idType } from "@/shared/types";
import { Button } from "@/shared/ui/Button";
import { Modal } from "@/shared/ui/Modal";

import cn from "./DeleteContactModal.module.css";

export interface DeleteContactModalProps {
  id: idType;
}

const DeleteContactModal: React.FC<DeleteContactModalProps> = ({ id }) => {
  const [visible, setVisible] = useState(false);
  const handleVisibleChange = () => setVisible(!visible);
  const [mutation, state] = useDeleteContactMutation();

  const handleDelete = async () => {
    try {
      await mutation(id).unwrap();
      handleVisibleChange();
    } catch (e) {
      console.error(e);
    }
  };
  return (
    <>
      <Button size={"small"} onClick={handleVisibleChange}>
        🗑️
      </Button>
      <Modal visible={visible} handleVisibleChange={handleVisibleChange}>
        <h3>Are you sure?</h3>
        <div className={classNames(cn.actions)}>
          <Button
            theme={"secondary"}
            onClick={handleDelete}
            isLoading={state.isLoading}
            disabled={state.isLoading}
          >
            Yes
          </Button>
          <Button theme={"text"} onClick={handleVisibleChange}>
            No
          </Button>
        </div>
      </Modal>
    </>
  );
};

export default DeleteContactModal;
