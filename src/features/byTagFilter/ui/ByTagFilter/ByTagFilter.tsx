import React from "react";
import classNames from "classnames";
import { useSearchParams } from "react-router-dom";

import { Tag, useGetAllTagsQuery } from "@/entities/Tag";

import cn from "./ByTagFilter.module.css";

export interface FilterByTagProps {}

const ByTagFilter: React.FC<FilterByTagProps> = () => {
  const [searchParams, setSearchParams] = useSearchParams();

  const { data } = useGetAllTagsQuery();

  const isTagExists = searchParams.get("tag_id");

  return (
    <div className={classNames(cn.wrapper)}>
      {data?.map((tag) => {
        const isTagSelected = isTagExists && isTagExists === tag.id?.toString();
        const handleClick = () => {
          isTagSelected
            ? searchParams.delete("tag_id")
            : searchParams.set("tag_id", tag.id as string);

          setSearchParams(searchParams);
        };

        return (
          <Tag
            theme={isTagSelected ? "secondary" : "primary"}
            key={tag.id}
            className={classNames(cn.block)}
            onClick={handleClick}
            tag={tag}
          />
        );
      })}
    </div>
  );
};

export default ByTagFilter;
