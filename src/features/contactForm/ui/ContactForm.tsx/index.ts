import ContactForm, { ContactFormProps } from "./ContactForm";

export type { ContactFormProps };
export { ContactForm };
