import React, { useEffect, useState } from "react";
import { useNavigate } from "react-router-dom";

import {
  Contact,
  useCreateContactMutation,
  useGetContactByIdQuery,
  useUpdateContactMutation,
} from "@/entities/Contact";
import { CreateTagModal } from "@/features/createTag";
import { SelectTag } from "@/features/selectTag";
import { idType } from "@/shared/types";
import { Button } from "@/shared/ui/Button";
import { FormField } from "@/shared/ui/FormField";
import { Input } from "@/shared/ui/Input";
import { Spinner } from "@/shared/ui/Spinner";

export interface ContactFormProps {
  id?: idType;
}

const ContactForm: React.FC<ContactFormProps> = ({ id }) => {
  const navigate = useNavigate();
  const { isLoading: isFetching, data } = useGetContactByIdQuery(id!, {
    skip: !id,
  });

  const [values, setValues] = useState<Partial<Contact>>({
    id: undefined,
    name: "",
    phone: "",
    tag_id: undefined,
    email: "",
  });
  const handleChangeValue = (
    e: React.ChangeEvent<HTMLInputElement | HTMLSelectElement>,
  ) => {
    setValues({ ...values, [e.target.name]: e.target.value });
  };

  useEffect(() => {
    if (data) {
      setValues({
        email: data.email,
        phone: data.phone,
        name: data.name,
        id: data.id,
        tag_id: data?.tag_id,
      });
    }
  }, [data]);

  const [createMutation, createState] = useCreateContactMutation();
  const [updateMutation, updateState] = useUpdateContactMutation();
  const isLoading = createState.isLoading || updateState.isLoading;

  if (isFetching) return <Spinner />;

  const onSubmit = async (e: React.FormEvent) => {
    e.preventDefault();

    const handler = data ? updateMutation : createMutation;

    try {
      await handler(values).unwrap();
      navigate("/");
    } catch (e) {
      console.error(e);
    }
  };

  return (
    <form onSubmit={onSubmit}>
      <FormField label={"Name:"}>
        <Input
          required
          name={"name" as keyof Contact}
          placeholder={"Input name..."}
          value={values?.name}
          onChange={handleChangeValue}
        />
      </FormField>
      <FormField label={"Phone:"}>
        <Input
          required
          name={"phone" as keyof Contact}
          placeholder={"Input phone..."}
          value={values?.phone}
          onChange={handleChangeValue}
        />
      </FormField>
      <FormField label={"Email:"}>
        <Input
          type={"email"}
          required
          name={"email" as keyof Contact}
          placeholder={"Input email..."}
          value={values?.email}
          onChange={handleChangeValue}
        />
      </FormField>

      <FormField>
        <SelectTag
          name={"tag_id" as keyof Contact}
          placeholder={"Choose tag..."}
          value={values.tag_id}
          onChange={handleChangeValue}
        />
      </FormField>

      <FormField>
        <CreateTagModal />
      </FormField>

      <Button
        type={"submit"}
        theme={"secondary"}
        disabled={isLoading}
        isLoading={isLoading}
      >
        Submit
      </Button>
    </form>
  );
};

export default ContactForm;
