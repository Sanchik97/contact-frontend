import React, { useState } from "react";
import { useNavigate } from "react-router-dom";

import {
  RegisterData,
  useLoginMutation,
  useRegisterMutation,
} from "@/entities/User";
import { setToLocalStorage } from "@/shared/lib/localStorage";
import { Button } from "@/shared/ui/Button";
import { FormField } from "@/shared/ui/FormField";
import { Input } from "@/shared/ui/Input";

export interface RegisterFormProps {}

const RegisterForm: React.FC<RegisterFormProps> = () => {
  const navigate = useNavigate();
  const [values, setValues] = useState<RegisterData>({
    email: "",
    password: "",
    name: "",
  });

  const handleChangeValue = (e: React.ChangeEvent<HTMLInputElement>) =>
    setValues({
      ...values,
      [e.target.name]: e.target.value,
    });

  const [registerMutation, registerState] = useRegisterMutation();
  const [loginMutation, loginState] = useLoginMutation();

  const onSubmit = async (e: React.FormEvent) => {
    e.preventDefault();
    try {
      await registerMutation(values).unwrap();

      const login = await loginMutation({
        email: values.email,
        password: values.password,
      }).unwrap();

      setToLocalStorage("auth-token", login.access_token);
      navigate("/", { replace: true });
    } catch (e) {
      console.error(e);
    }
  };

  return (
    <form onSubmit={onSubmit}>
      <FormField label={"Name:"}>
        <Input
          onChange={handleChangeValue}
          value={values.name}
          name={"name" as keyof RegisterData}
          placeholder={"Input login..."}
          disabled={registerState.isLoading}
          required
        />
      </FormField>
      <FormField label={"Email:"}>
        <Input
          type={"email"}
          onChange={handleChangeValue}
          value={values.email}
          name={"email" as keyof RegisterData}
          placeholder={"Input email..."}
          disabled={registerState.isLoading}
          required
        />
      </FormField>

      <FormField label={"password:"}>
        <Input
          type={"password"}
          onChange={handleChangeValue}
          value={values.password}
          name={"password" as keyof RegisterData}
          placeholder={"Input password..."}
          disabled={registerState.isLoading}
          required
        />
      </FormField>

      <div>
        {registerState.isError ? "An error has occurred..." : undefined}
      </div>

      <Button
        type={"submit"}
        disabled={registerState.isLoading || loginState.isLoading}
        isLoading={registerState.isLoading || loginState.isLoading}
      >
        Register
      </Button>
    </form>
  );
};

export default RegisterForm;
