import React, { useState } from "react";
import { useNavigate } from "react-router-dom";

import { LoginData, useLoginMutation } from "@/entities/User";
import { setToLocalStorage } from "@/shared/lib/localStorage";
import { Button } from "@/shared/ui/Button";
import { FormField } from "@/shared/ui/FormField";
import { Input } from "@/shared/ui/Input";

export interface LoginFormProps {}

const LoginForm: React.FC<LoginFormProps> = () => {
  const navigate = useNavigate();
  const [mutation, state] = useLoginMutation();
  const [values, setValues] = useState<LoginData>({
    email: "",
    password: "",
  });
  const handleChangeValue = (e: React.ChangeEvent<HTMLInputElement>) =>
    setValues({
      ...values,
      [e.target.name]: e.target.value,
    });

  const onSubmit = async (e: React.FormEvent) => {
    e.preventDefault();
    try {
      const response = await mutation(values).unwrap();
      setToLocalStorage("auth-token", response.access_token);
      navigate("/");
    } catch (e) {
      console.error(e);
    }
  };
  return (
    <form onSubmit={onSubmit}>
      <FormField label={"Email:"}>
        <Input
          type={"email"}
          onChange={handleChangeValue}
          value={values.email}
          name={"email" as keyof LoginData}
          placeholder={"Input email..."}
          required
        />
      </FormField>

      <FormField label={"password:"}>
        <Input
          type={"password"}
          onChange={handleChangeValue}
          value={values.password}
          name={"password" as keyof LoginData}
          placeholder={"Input password..."}
          required
        />
      </FormField>

      <div>{state.isError ? "An error has occurred..." : undefined}</div>

      <Button
        type={"submit"}
        disabled={state.isLoading}
        isLoading={state.isLoading}
      >
        Enter
      </Button>
    </form>
  );
};

export default LoginForm;
