import React from "react";
import { createRoot } from "react-dom/client";

import App from "@/app";
import { StoreProvider } from "@/app/providers/StoreProvider";

import "@/app/styles/global.css";

createRoot(document.getElementById("root") as HTMLElement).render(
  <StoreProvider>
    <App />
  </StoreProvider>,
);
