import MainLayout, { MainLayoutProps } from "./ui/MainLayout/MainLayout";

export type { MainLayoutProps };
export { MainLayout };
