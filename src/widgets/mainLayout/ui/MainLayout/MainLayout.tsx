import React, { Suspense } from "react";
import classNames from "classnames";
import { Link, Outlet } from "react-router-dom";

import { Container } from "@/shared/ui/Container";
import { Spinner } from "@/shared/ui/Spinner";

import cn from "./MainLayout.module.css";

export interface MainLayoutProps {}

const MainLayout: React.FC<MainLayoutProps> = () => {
  return (
    <div className={classNames(cn.layout)}>
      <header className={classNames(cn.header)}>
        <Container>
          <div className={classNames(cn.headerWrapper)}>
            <Link className={classNames(cn.logo)} to={"/"}>
              <img
                className={classNames(cn.logoImage)}
                src={"/logo.svg"}
                alt={""}
              />
              Contacts
            </Link>
          </div>
        </Container>
      </header>
      <main className={classNames(cn.main)}>
        <Suspense fallback={<Spinner />}>
          <Outlet />
        </Suspense>
      </main>
    </div>
  );
};

export default MainLayout;
