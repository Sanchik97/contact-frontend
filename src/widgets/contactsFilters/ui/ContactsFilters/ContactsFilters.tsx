import React from "react";
import classNames from "classnames";

import { ByInputFilter } from "@/features/byInputFilter";
import { ByTagFilter } from "@/features/byTagFilter";

import cn from "./ContactsFilters.module.css";

export interface ContactsFiltersProps {}

const ContactsFilters: React.FC<ContactsFiltersProps> = () => {
  return (
    <div className={classNames(cn.sticky)}>
      <ByInputFilter />
      <ByTagFilter />
    </div>
  );
};

export default ContactsFilters;
