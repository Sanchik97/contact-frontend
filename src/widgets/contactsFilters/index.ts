import { ContactsFilters, ContactsFiltersProps } from "./ui/ContactsFilters";

export type { ContactsFiltersProps };
export { ContactsFilters };
