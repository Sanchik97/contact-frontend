import { AuthWidget } from "./ui/AuthWidget";
import { AuthWidgetProps } from "./ui/AuthWidget/AuthWidget";

export type { AuthWidgetProps };
export { AuthWidget };
