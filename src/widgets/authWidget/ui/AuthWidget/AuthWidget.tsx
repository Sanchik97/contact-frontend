import React, { useState } from "react";
import classNames from "classnames";

import { LoginForm } from "@/features/login";
import { RegisterForm } from "@/features/register";

import cn from "./AuthWidget.module.css";

export interface AuthWidgetProps {}

const AuthWidget: React.FC<AuthWidgetProps> = () => {
  const [isRegister, setIsRegister] = useState(false);

  const setLogin = () => setIsRegister(false);
  const setRegister = () => setIsRegister(true);

  return (
    <div>
      <div className={classNames(cn.header)}>
        <h2
          className={classNames(cn.title, { [cn.active]: !isRegister })}
          onClick={setLogin}
        >
          Sign In
        </h2>
        <span>/</span>
        <h2
          className={classNames(cn.title, { [cn.active]: isRegister })}
          onClick={setRegister}
        >
          Register
        </h2>
      </div>
      {isRegister ? <RegisterForm /> : <LoginForm />}
    </div>
  );
};

export default AuthWidget;
