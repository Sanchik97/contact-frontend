import ContactsList, {
  ContactsListProps,
} from "./ui/contactsList/contactsList";

export type { ContactsListProps };
export { ContactsList };
