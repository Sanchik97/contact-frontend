import React from "react";
import classNames from "classnames";
import { useSearchParams } from "react-router-dom";

import { ContactInfo, useGetAllContactsQuery } from "@/entities/Contact";
import { ErrorInfo } from "@/shared/ui/ErrorInfo";
import { Spinner } from "@/shared/ui/Spinner";

import cn from "./contactsList.module.css";

export interface ContactsListProps {}

const ContactsList: React.FC<ContactsListProps> = () => {
  const [searchParams] = useSearchParams();
  const { isLoading, isError, data } = useGetAllContactsQuery({
    search: searchParams.get("search") ?? "",
    tag_id: searchParams.get("tag_id") ?? "",
  });

  if (isLoading) return <Spinner />;
  if (isError) return <ErrorInfo />;

  return (
    <div className={classNames(cn.list)}>
      {data?.length
        ? data.map((contact) => (
            <ContactInfo contact={contact} key={contact.id} />
          ))
        : "Empty list..."}
    </div>
  );
};

export default ContactsList;
