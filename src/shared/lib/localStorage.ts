type LocalStorageKeys = "auth-token" | "products";

const getFromLocalStorage = (key: LocalStorageKeys) => {
  return localStorage.getItem(key);
};

const setToLocalStorage = <T>(key: LocalStorageKeys, entity: T) => {
  return localStorage.setItem(
    key,
    typeof entity === "string" ? entity : JSON.stringify(entity),
  );
};

const removeFromLocalStorage = (key: LocalStorageKeys) => {
  return localStorage.removeItem(key);
};

export { getFromLocalStorage, removeFromLocalStorage, setToLocalStorage };
