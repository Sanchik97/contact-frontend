import React, { ButtonHTMLAttributes, DetailedHTMLProps } from "react";
import classNames from "classnames";
import { Link } from "react-router-dom";

import cn from "./Button.module.css";

type ButtonType = "primary" | "secondary" | "text";
type ButtonSize = "medium" | "small" | "large";

export interface ButtonProps
  extends DetailedHTMLProps<
    ButtonHTMLAttributes<HTMLButtonElement>,
    HTMLButtonElement
  > {
  className?: string;
  theme?: ButtonType;
  block?: boolean;
  children?: React.ReactNode;
  isLoading?: boolean;
  href?: string;
  size?: ButtonSize;
}

const Button: React.FC<ButtonProps> = ({
  className,
  theme = "primary",
  block = false,
  children,
  isLoading,
  href,
  size = "medium",
  ...rest
}) => {
  const classes = classNames(
    cn.button,
    {
      [cn[theme]]: true,
      [cn.block]: block,
    },
    [className, cn[size]],
  );

  if (href)
    return (
      <Link to={href} className={classes}>
        {children}
      </Link>
    );

  return (
    <button type={"button"} className={classes} {...rest}>
      {isLoading ? "Loading..." : children}
    </button>
  );
};

export default Button;
