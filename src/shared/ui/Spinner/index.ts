import Spinner, { SpinnerProps } from "./Spinner";

export type { SpinnerProps };

export { Spinner };
