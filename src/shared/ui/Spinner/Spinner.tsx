import React from "react";

import { Container } from "@/shared/ui/Container";

export interface SpinnerProps {}

const Spinner: React.FC<SpinnerProps> = () => {
  return <Container>Loading...</Container>;
};

export default Spinner;
