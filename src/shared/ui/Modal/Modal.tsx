import React, { useCallback, useEffect } from "react";
import classNames from "classnames";
import { createPortal } from "react-dom";

import { Button } from "@/shared/ui/Button";

import cn from "./Modal.module.css";

export interface ModalProps {
  className?: string;
  children?: React.ReactNode;
  visible?: boolean;
  handleVisibleChange?: () => void;
}

const Modal: React.FC<ModalProps> = ({
  className,
  children,
  visible,
  handleVisibleChange,
}) => {
  const onChange = useCallback(() => {
    if (visible) {
      document.body.classList.remove("overflow-hidden");
      handleVisibleChange?.();
    }
  }, [handleVisibleChange, visible]);

  useEffect(() => {
    if (visible) document.body.classList.add("overflow-hidden");
    else document.body.classList.remove("overflow-hidden");
  }, [visible]);

  if (!visible) return <React.Fragment />;

  return createPortal(
    <div>
      <div
        className={classNames(cn.modal, { [cn.show]: visible }, [className])}
      >
        <div className={classNames(cn.overlay)} onClick={onChange} />
        <div className={classNames(cn.content, {}, ["modal__content"])}>
          <Button
            theme={"secondary"}
            size={"small"}
            className={classNames(cn.closeBtn)}
            onClick={onChange}
          >
            Close
          </Button>
          <div className={classNames(cn.body)}>{children}</div>
        </div>
      </div>
    </div>,
    document.body,
  );
};

export default Modal;
