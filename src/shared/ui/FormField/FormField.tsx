import React from "react";
import classNames from "classnames";

import cn from "./FormField.module.css";

export interface FormFieldProps {
  label?: string;
  children: React.ReactElement;
}

const FormField: React.FC<FormFieldProps> = ({ label, children }) => {
  const id = React.useId();
  const Input = React.cloneElement(children, { id });

  return (
    <div className={classNames(cn.field)}>
      {label && (
        <label className={classNames(cn.label)} htmlFor={id}>
          {label}
        </label>
      )}
      {Input}
    </div>
  );
};

export default FormField;
