import React from "react";
import classNames from "classnames";

import cn from "./Input.module.css";

export interface InputProps
  extends React.DetailedHTMLProps<
    React.InputHTMLAttributes<HTMLInputElement>,
    HTMLInputElement
  > {}

const Input: React.FC<InputProps> = ({ className, ...rest }) => {
  return <input className={classNames(cn.input, {}, [className])} {...rest} />;
};

export default Input;
