import ErrorInfo, { ErrorInfoProps } from "./ErrorInfo";

export type { ErrorInfoProps };

export { ErrorInfo };
