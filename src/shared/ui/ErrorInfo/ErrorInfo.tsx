import React from "react";

import { Container } from "@/shared/ui/Container";

export interface ErrorInfoProps {}

const ErrorInfo: React.FC<ErrorInfoProps> = () => {
  return (
    <Container>
      <h3>An error has occurred..</h3>
      <p>Please reload the page.</p>
    </Container>
  );
};

export default ErrorInfo;
