import React from "react";
import classNames from "classnames";

import cn from "./Select.module.css";

export interface SelectProps
  extends React.DetailedHTMLProps<
    React.SelectHTMLAttributes<HTMLSelectElement>,
    HTMLSelectElement
  > {
  className?: string;
}

const Select: React.FC<SelectProps> = ({ className, children, ...rest }) => {
  return (
    <select className={classNames(cn.select, {}, [className])} {...rest}>
      {children}
    </select>
  );
};

export default Select;
