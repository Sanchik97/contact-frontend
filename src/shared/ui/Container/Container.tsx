import React from "react";
import classNames from "classnames";

import cn from "./Container.module.css";

export interface ContainerProps {
  children: React.ReactNode;
}

const Container: React.FC<ContainerProps> = ({ children }) => {
  return <div className={classNames(cn.container)}>{children}</div>;
};

export default Container;
