import React from "react";
import classNames from "classnames";

import cn from "./NotFound.module.css";

export interface NotFoundProps {}

const NotFound: React.FC<NotFoundProps> = () => {
  return (
    <div className={classNames(cn.block)}>
      <img
        className={classNames(cn.image)}
        src={"/not-found.svg"}
        alt={"404"}
      />
      <p className={cn.text}>Page not found...</p>
    </div>
  );
};

export default NotFound;
