import { baseQuery } from "@/shared/api/baseQuery";
import { createApi } from "@reduxjs/toolkit/query/react";

import {
  LoginData,
  LoginResponse,
  RegisterData,
  User,
} from "../model/userTypes";

export const userApi = createApi({
  reducerPath: "userApi",
  baseQuery,
  endpoints: (build) => ({
    getUser: build.query<User, void>({
      query: () => "/api/auth/me",
    }),
    login: build.mutation<LoginResponse, LoginData>({
      query: (data) => {
        return {
          url: "/api/login",
          method: "POST",
          body: data,
        };
      },
    }),
    register: build.mutation<User, RegisterData>({
      query: (data) => {
        return {
          url: "/api/register",
          method: "POST",
          body: data,
        };
      },
    }),
    logout: build.mutation<void, void>({
      query: () => {
        return {
          url: "/api/logout",
          method: "POST",
        };
      },
    }),
  }),
});

export const { useGetUserQuery, useLoginMutation, useRegisterMutation } =
  userApi;
