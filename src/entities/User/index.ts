import {
  useGetUserQuery,
  useLoginMutation,
  userApi,
  useRegisterMutation,
} from "./api/userApi";
import { LoginData, RegisterData, User } from "./model/userTypes";

export type { LoginData, RegisterData, User };
export { useGetUserQuery, useLoginMutation, userApi, useRegisterMutation };
