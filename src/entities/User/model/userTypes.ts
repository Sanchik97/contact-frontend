import { idType } from "@/shared/types";

export type LoginData = {
  email: string;
  password: string;
};
export type RegisterData = {
  email: string;
  password: string;
  name: string;
};

export type LoginResponse = {
  user: User;
  access_token: string;
};

export interface User {
  id: idType;
  created_at: string;
  email: string;
  password: string;
  name: string;
}
