import React from "react";
import classNames from "classnames";

import { DeleteContactModal } from "@/features/deleteContact";
import { Button } from "@/shared/ui/Button";

import { Contact } from "../../model/contactTypes";

import cn from "./ContactInfo.module.css";

export interface ContactInfoProps {
  contact: Contact;
}

const ContactInfo: React.FC<ContactInfoProps> = ({ contact }) => {
  return (
    <div className={classNames(cn.block)}>
      <div className={classNames(cn.info)}>
        <div className={classNames(cn.header)}>
          <p className={classNames(cn.name)}>{contact.name}</p>
          <div className={classNames(cn.actions)}>
            <Button size={"small"} href={"/contacts/edit/" + contact.id}>
              ✍
            </Button>
            <DeleteContactModal id={contact.id} />
          </div>
        </div>
        <p className={classNames(cn.phone)}>
          <span>📱</span>
          {contact.phone}
        </p>
        <a href={"mailto:" + contact.email} className={classNames(cn.email)}>
          <span>✉️</span>
          {contact.email}
        </a>
      </div>
      {contact.tag && (
        <div className={classNames(cn.tagWrapper)}>
          <div className={classNames(cn.tag)}>{contact.tag?.name}</div>
        </div>
      )}
    </div>
  );
};

export default ContactInfo;
