import { TagModel } from "@/entities/Tag";
import { idType } from "@/shared/types";

export interface Contact {
  id: idType;
  name: string;
  phone: string;
  email: string;
  tag_id?: string;
  tag: TagModel | null;
}

export type ContactsQueryParams = {
  search: string;
  tag_id: idType;
};
