import { baseQuery } from "@/shared/api/baseQuery";
import { idType } from "@/shared/types";
import { createApi } from "@reduxjs/toolkit/query/react";

import { Contact, ContactsQueryParams } from "../model/contactTypes";

export const contactApi = createApi({
  reducerPath: "contactApi",
  tagTypes: ["Contact"],
  baseQuery,
  endpoints: (build) => ({
    getAllContacts: build.query<Contact[], ContactsQueryParams>({
      providesTags: ["Contact"],
      query: (params) => {
        return {
          url: "/api/contacts",
          params,
        };
      },
    }),
    getContactById: build.query<Contact, idType>({
      query: (id) => {
        return {
          url: "/api/contacts/" + id,
        };
      },
    }),

    createContact: build.mutation<Contact, Partial<Contact>>({
      query: (contact) => {
        return {
          url: "/api/contacts",
          method: "POST",
          body: contact,
        };
      },
      invalidatesTags: ["Contact"],
    }),
    updateContact: build.mutation<Contact, Partial<Contact>>({
      query: (contact) => {
        return {
          url: "/api/contacts/" + contact.id,
          method: "PATCH",
          body: contact,
        };
      },
      invalidatesTags: ["Contact"],
    }),
    deleteContact: build.mutation<void, idType>({
      query: (id) => {
        return {
          url: "/api/contacts/" + id,
          method: "DELETE",
        };
      },
      invalidatesTags: ["Contact"],
    }),
  }),
});

export const {
  useGetAllContactsQuery,
  useGetContactByIdQuery,
  useUpdateContactMutation,
  useCreateContactMutation,
  useDeleteContactMutation,
} = contactApi;
