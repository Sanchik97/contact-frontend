import {
  contactApi,
  useCreateContactMutation,
  useDeleteContactMutation,
  useGetAllContactsQuery,
  useGetContactByIdQuery,
  useUpdateContactMutation,
} from "./api/contactApi";
import { Contact, ContactsQueryParams } from "./model/contactTypes";
import { ContactInfo } from "./ui/ContactInfo";

export type { Contact, ContactsQueryParams };
export {
  contactApi,
  ContactInfo,
  useCreateContactMutation,
  useDeleteContactMutation,
  useGetAllContactsQuery,
  useGetContactByIdQuery,
  useUpdateContactMutation,
};
