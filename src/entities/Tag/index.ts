import {
  tagApi,
  useCreateTagMutation,
  useDeleteContactMutation,
  useGetAllTagsQuery,
  useUpdateContactMutation,
} from "./api/tagApi";
import { TagModel } from "./model/tagTypes";
import { Tag, TagProps } from "./ui/Tag";

export type { TagModel, TagProps };
export {
  Tag,
  tagApi,
  useCreateTagMutation,
  useDeleteContactMutation,
  useGetAllTagsQuery,
  useUpdateContactMutation,
};
