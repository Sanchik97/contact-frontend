import React from "react";
import classNames from "classnames";

import { Button, ButtonProps } from "@/shared/ui/Button";

import { TagModel } from "../../model/tagTypes";

import cn from "./Tag.module.css";

export interface TagProps extends ButtonProps {
  tag?: TagModel;
  className?: string;
}

const Tag: React.FC<TagProps> = ({ tag, className, ...rest }) => {
  return (
    <Button
      size={"small"}
      className={classNames(cn.tag, {}, [className])}
      {...rest}
    >
      {tag?.name}
    </Button>
  );
};

export default Tag;
