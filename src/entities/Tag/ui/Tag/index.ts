import Tag, { TagProps } from "./Tag";

export type { TagProps };
export { Tag };
