import { Contact } from "@/entities/Contact";
import { baseQuery } from "@/shared/api/baseQuery";
import { idType } from "@/shared/types";
import { createApi } from "@reduxjs/toolkit/query/react";

import { TagModel } from "../model/tagTypes";

export const tagApi = createApi({
  tagTypes: ["Tag"],
  reducerPath: "tagApi",
  baseQuery,
  endpoints: (build) => ({
    getAllTags: build.query<TagModel[], void>({
      query: () => {
        return {
          url: "/api/tags",
        };
      },
    }),

    createTag: build.mutation<Contact, string>({
      query: (name) => {
        return {
          url: "/api/tags",
          method: "POST",
          body: { name },
        };
      },
      invalidatesTags: ["Tag"],
    }),
    updateContact: build.mutation<Contact, Partial<TagModel>>({
      query: (tag) => {
        return {
          url: "/api/tags/" + tag.id,
          method: "PATCH",
          body: tag,
        };
      },
      invalidatesTags: ["Tag"],
    }),

    deleteContact: build.mutation<void, idType>({
      query: (id) => {
        return {
          url: "/api/tags/" + id,
          method: "DELETE",
        };
      },
      invalidatesTags: ["Tag"],
    }),
  }),
});

export const {
  useGetAllTagsQuery,
  useUpdateContactMutation,
  useDeleteContactMutation,
  useCreateTagMutation,
} = tagApi;
