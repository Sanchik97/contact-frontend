import { idType } from "@/shared/types";

export interface TagModel {
  id: idType;
  name: string;
  createdAt: string;
}
