import React from "react";
import { RouterProvider as Provider } from "react-router-dom";

import routes from "../config/routes";

interface props {}

const RouterProvider: React.FC<props> = () => {
  return <Provider router={routes} />;
};

export default RouterProvider;
