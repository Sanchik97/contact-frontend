import React from "react";
import { createBrowserRouter, redirect } from "react-router-dom";

import { getFromLocalStorage } from "@/shared/lib/localStorage";
import { MainLayout } from "@/widgets/mainLayout";

const HomePage = React.lazy(() => import("@/pages/HomePage"));
const NotFoundPage = React.lazy(() => import("@/pages/NotFoundPage"));
const AuthPage = React.lazy(() => import("@/pages/AuthPage"));
const CreateContactPage = React.lazy(() => import("@/pages/CreateContactPage"));
const EditContactPage = React.lazy(() => import("@/pages/EditContactPage"));

export default createBrowserRouter([
  {
    element: <MainLayout />,
    children: [
      {
        path: "/",
        element: <HomePage />,
        loader: () => {
          const token = getFromLocalStorage("auth-token");
          if (!token) {
            throw redirect("/auth");
          }
          return null;
        },
      },
      {
        path: "/auth",
        element: <AuthPage />,
        loader: () => {
          if (getFromLocalStorage("auth-token")) {
            throw redirect("/");
          }
          return null;
        },
      },
      {
        path: "/contacts/add",
        element: <CreateContactPage />,
      },
      {
        path: "/contacts/edit/:id",
        element: <EditContactPage />,
      },
      {
        path: "*",
        element: <NotFoundPage />,
      },
    ],
  },
]);
