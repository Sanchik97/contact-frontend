import React from "react";
import { Provider } from "react-redux";

import { store } from "../config/index";

interface props {
  children: React.ReactNode;
}

const StoreProvider: React.FC<props> = ({ children }) => {
  return <Provider store={store}>{children}</Provider>;
};

export default StoreProvider;
