import StoreProvider from "./ui/StoreProvider";
import { AppDispatch, RootState } from "./config";

export type { AppDispatch, RootState };

export { StoreProvider };
