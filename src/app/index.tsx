import React from "react";

import { RouterProvider } from "./providers/RouterProvider";

interface props {}

const App: React.FC<props> = () => {
  return <RouterProvider />;
};

export default App;
