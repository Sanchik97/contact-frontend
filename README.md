## Contacts App

https://contact-frontend-psi.vercel.app

### Стек технологий:

- **React** - 18.2.0
- **Typescript** - 4.7.4
- **React Router Dom** - 6.14.2
- **@reduxjs/toolkit** - 1.9.5
- **Feature Sliced Design** - 2.0-beta

---

### Docker

### Сборка и Запуск

```
docker compose up --build
```
